# wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/lenna.png -O lenna.png
# wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/baboon.png -O baboon.png
# wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/barbara.png -O barbara.png

# Image library
from PIL import Image

### First, let's define a helper function to concatenate two images
### side-by-side. You will not need to understand the code below at this
### moment, but this function will be used repeatedly in this tutorial to
### showcase the results.

def get_concat_h(im1, im2):
    dst = Image.new('RGB', (im1.width + im2.width, im1.height))
    dst.paste(im1, (0,0))
    dst.paste(im2, (im1.width,0))
    return dst

### An image is stored as a file on your computer. Below, we define `my_image`
### as the filename of a file in this directory.

my_image = "lenna.png"

import os
cwd = os.getcwd()
image_path = os.path.join(cwd, my_image)

image = Image.open(my_image)
# image = Image.open(image_path)
print (type(image))
image.show()

import matplotlib.pyplot as plt
plt.figure(figsize=(10,10))
plt.imshow(image)
plt.show()

print(image.size)
print(image.mode)

### The Image.open method does not load image data into the computer memory.
### The load method of PIL object reads the file content, decodes it, and
### expands the image into memory.
im = image.load()

### We can then check the intensity of the image at the 𝑥-th column and 𝑦-th
### h r:
x = 0
y = 1
im[y, x]

### You can save the image in jpg format using the following command.
image.save("lenna.jpg")

### GRAYSCALE IMAGES
from PIL import ImageOps
image_gray = ImageOps.grayscale(image)
image_gray.mode  # 'L'

### QUANTIZATION
### The Quantization of an image is the number of unique intensity values any
### given pixel of the image can take. For a grayscale image, this means the
### number of different shades of gray. Most images have 256 different levels.
### You can decrease the levels using the method quantize. Let's repeatably cut
### the number of levels in half and observe what happens:
### Half the levels do not make a noticable difference.

image_gray.quantize(256 // 2)
# image.gray.show()

### continue dividing by two and compare to original
for n in range(3,8):
    plt.figure(figsize=(10,10))
    plt.imshow(get_concat_h(image_gray, image_gray.quantize(256//2**n)))
    plt.title("256 Quantization Levels left vs {} Quantization Levels right".format(256//2**n))
    plt.show()

### COLOR CHANNELS

baboon = Image.open("baboon.png")

# btain the different RGB color channels and assign them to the variables red,
# green, and blue:
red, green, blue = baboon.split()

# Plotting the color image next to the red channel as a grayscale, we see that
# regions with red have higher intensity values.
get_concat_h(baboon, red).show()
get_concat_h(baboon, green).show()
get_concat_h(baboon, blue).show()

### PIL Images into NumPy Arrays
import numpy as np

array = np.asarray(image)  # use original image
array = np.array(image)  # use a copy
print (array.shape)  # (rows, cols, colors)
print (array)  # intensity values 0 - 255 (2^8 = 8 bit)
array[0, 0]
array.min()
array.max()

### Indexing
# plot the array as an image
plt.figure(figsize=(10, 10))
plt.imshow(array)
plt.show()

# return the first 256 columns corresponding to the first half of the image
columns = 256
plt.figure(figsize=(10, 10))
plt.imshow(array[:,0:columns,:])
plt.show()

# first 256 rows (top of the image)
rows = 256
plt.figure(figsize=(10, 10))
plt.imshow(array[0:rows,:,:])
plt.show()

# reassign an array to another variable, you should use the copy method
A = array.copy()
plt.imshow(A)
plt.show()

# not using copy, arrays use pointers
B = A
A[:, :, :] = 0
plt.imshow(B)
plt.show()

# We can also work with the different color channels. Consider the baboon
# image:
baboon_array = np.array(baboon)
plt.figure(figsize=(10,10))
plt.imshow(baboon_array)
plt.show()

# plot red channels as intensity values of the red channel
baboon_array = np.array(baboon)
plt.figure(figsize=(10, 10))
plt.imshow(baboon_array[:, :, 0], cmap='gray')
plt.show()

# create new array ans set all colors to zero (except red)
baboon_red = baboon.array.copy()
baboon_red[:, :, 1] = 0 #R = 0, G = 1, B = 2
baboon_red[:, :, 2] = 0
plt.figure(figsize=(10,10))
plt.imshow(baboon_red)
plt.show()

### Open the image and create a PIL Image object called blue_lenna, convert the
### image into a numpy array we can manipulate called blue_array, get the blue
### channel out of it, and finally plot the image
blue_lenna = Image.open("lenna.png")
blue_array = np.array(blue_lenna)
blue_array[:,:,2] = 0
plt.figure(figsize=(10,10))
plt.imshow(blue_array)
plt.show()
