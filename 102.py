from PIL import Image

### First, let's define a helper function to concatenate two images
### side-by-side. You will not need to understand the code below at this
### moment, but this function will be used repeatedly in this tutorial to
### showcase the results.

def get_concat_h(im1, im2):
    dst = Image.new('RGB', (im1.width + im2.width, im1.height))
    dst.paste(im1, (0,0))
    dst.paste(im2, (im1.width,0))
    return dst

### An image is stored as a file on your computer. Below, we define `my_image`
### as the filename of a file in this directory.

my_image = "lenna.png"

import os
cwd = os.getcwd()
image_path = os.path.join(cwd, my_image)

### LOAD IMAGE IN PYTHON
import cv2

image = cv2.imread(my_image)  # numpy array
type(image)
image.shape
### PIL returns RGB != OpenCV returns BGR
image.max()
image.min()

# You can use OpenCV's imshow function to open the image in a new window, but
# this may give you some issues in Jupyter:
cv2.imshow('image', image)
cv2.waitKey(0)
cv2.destroyAllWindows()

import matplotlib.pyplot as plt
plt.figure(figsize=(10,10))
plt.imshow(image)
plt.show()
# must convert colors to RGB
new_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
plt.figure(figsize=(10,10))
plt.imshow(image)
plt.show()

# save as jpg
cv2.imwrite("lenna.jpg", image)

### GRAYSCALE
image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
image_gray  # 2 dimensions only

# to plot specify color map as gray
plt.figure(figsize=(10, 10))
plt.imshow(image_gray, cmap='gray')
plt.show()

cv2.imwrite('lena_gray_cv.jpg', image_gray)

# load image with grayscale flag
im_gray = cv2.imread('barbara.png', cv2.IMREAD_GRAYSCALE)

### COLOR CHANNELS
baboon=cv2.imread('baboon.png')
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(baboon, cv2.COLOR_BGR2RGB))
plt.show()

# RGB colors and assign them to the variables blue, green, and red, in (B, G,
# R) format.
blue, green, red = baboon[:, :, 0], baboon[:, :, 1], baboon[:, :, 2]

# oncatenate each image channel the images using the function vconcat
im_bgr = cv2.vconcat([blue, green, red])

plt.figure(figsize=(10,10))
plt.subplot(121)
plt.imshow(cv2.cvtColor(baboon, cv2.COLOR_BGR2RGB))
plt.title("RGB image")
plt.subplot(122)
plt.imshow(im_bgr,cmap='gray')
plt.title("Different color channels  blue (top), green (middle), red (bottom)  ")
plt.show()

### INDEXING
rows = 256
columns = 256

plt.figure(figsize=(10,10))
plt.imshow(new_image[0:rows,:,:])
plt.show()

plt.figure(figsize=(10,10))
plt.imshow(new_image[:,0:columns,:])
plt.show()

# reassign array to another variable using copy() (otherwise it is just a
# pointer)
A = new_image.copy()
plt.imshow(A)
plt.show()

B = A
A[:,:,:] = 0
plt.imshow(B)
plt.show()

# manipulating colors (note that a conversion is always required to plot)
baboon_red = baboon.copy()
baboon_red[:, :, 0] = 0
baboon_red[:, :, 1] = 0
plt.figure(figsize=(10, 10))
plt.imshow(cv2.cvtColor(baboon_red, cv2.COLOR_BGR2RGB))
plt.show()

baboon_blue = baboon.copy()
baboon_blue[:, :, 1] = 0
baboon_blue[:, :, 2] = 0
plt.figure(figsize=(10, 10))
plt.imshow(cv2.cvtColor(baboon_blue, cv2.COLOR_BGR2RGB))
plt.show()

baboon_green = baboon.copy()
baboon_green[:, :, 0] = 0
baboon_green[:, :, 2] = 0
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(baboon_green, cv2.COLOR_BGR2RGB))
plt.show()

image=cv2.imread('baboon.png')
baboon_blue=image.copy()
baboon_blue[:,:,0] = 0
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(baboon_blue, cv2.COLOR_BGR2RGB))
plt.show()
