# wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/cat.png -O cat.png

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

baboon = np.array(Image.open('baboon.png'))
plt.figure(figsize=(5,5))
plt.imshow(baboon )
plt.show()

### FLIPPING IMAGES
image = Image.open("cat.png")
plt.figure(figsize=(10,10))
plt.imshow(image)
plt.show()

# cast image to array and find shape
array = np.array(image)
width, height, C = array.shape
print ('width, height, C', width, height, C)

# flip and rotate vertically. We assign the first row of pixels of the original
# array to the new array’s last row. We repeat the process for every row,
# incrementing the row number from the original array and decreasing the new
# array’s row index to assign the pixels accordingly. After excecuting the for
# loop below, array_flip will become the flipped image.
array_flip = np.zeros((width, height, C), dtype=np.uint8)
for i, row in enumerate(array):
    array_flip[width - 1 - i, :, :] = row

from PIL import ImageOps

im_flip = ImageOps.flip(image)
plt.figure(figsize=(5,5))
plt.imshow(im_flip)
plt.show()

im_mirror = ImageOps.mirror(image)
plt.figure(figsize=(5,5))
plt.imshow(im_mirror)
plt.show()

im_flip = image.transpose(1)
plt.imshow(im_flip)
plt.show()

# The Image module has built-in attributes that describe the type of flip. The
# values are just integers. Several are shown in the following dict:
flip = {"FLIP_LEFT_RIGHT": Image.FLIP_LEFT_RIGHT,
        "FLIP_TOP_BOTTOM": Image.FLIP_TOP_BOTTOM,
        "ROTATE_90": Image.ROTATE_90,
        "ROTATE_180": Image.ROTATE_180,
        "ROTATE_270": Image.ROTATE_270,
        "TRANSPOSE": Image.TRANSPOSE, 
        "TRANSVERSE": Image.TRANSVERSE}

for key, values in flip.items():
    plt.figure(figsize=(10,10))
    plt.subplot(1,2,1)
    plt.imshow(image)
    plt.title("orignal")
    plt.subplot(1,2,2)
    plt.imshow(image.transpose(values))
    plt.title(key)
    plt.show()

### CROPPING IMAGES
upper = 150
lower = 400
crop_top = array[upper:lower, :, :]
plt.figure(figsize=(5,5))
plt.imshow(crop_top)
plt.show()

left = 150
right = 400
crop_horizontal = array[:, left:right, :]
plt.figure(figsize=(5,5))
plt.imshow(crop_horizontal)
plt.show()

image = Image.open("cat.png")
crop_image = image.crop((left, upper, right, lower))
plt.figure(figsize=(5,5))
plt.imshow(crop_image)
plt.show()

crop_image = crop_image.transpose(Image.FLIP_LEFT_RIGHT)

### Changing specific image pixels

# change specific image pixels using array indexing; for example, we can set
# all the green and blue channels in the original image we cropped to zero:
array_sq = np.copy(array)
array_sq[upper:lower, left:right, 1:2] = 0

plt.figure(figsize=(5,5))
plt.subplot(1,2,1)
plt.imshow(array)
plt.title("orignal")
plt.subplot(1,2,2)
plt.imshow(array_sq)
plt.title("Altered Image")
plt.show()

### Also, ImageDraw from PIL
from PIL import ImageDraw

image_draw = image.copy()
image_fn = ImageDraw.Draw(im=image_draw)

shape = [left, upper, right, lower]
image_fn.rectangle(xy=shape, fill="red")

plt.figure(figsize=(10,10))
plt.imshow(image_draw)
plt.show()

### Adding text
from PIL import ImageFont

image_fn.text(xy=(0, 0), text="box", fill=(0, 0, 0))
plt.figure(figsize=(10,10))
plt.imshow(image_draw)
plt.show()

### Overlay
image_lenna = Image.open("lenna.png")
array_lenna = np.array(image_lenna)
array_lenna[upper:lower, left:right, :] = array[upper:lower, left:right, :]
plt.imshow(array_lenna)
plt.show()

# also works with paste()
image_lenna.paste(crop_image, box=(left, upper))
plt.imshow(image_lenna)
plt.show()

