import matplotlib.pyplot as plt
import cv2
import numpy as np

# copy to new array (reassigning only creates a pointer)

baboon = cv2.imread("baboon.png")
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(baboon, cv2.COLOR_BGR2RGB))
plt.show()

A = baboon
id(A)==id(baboon)
id(A)
B = baboon.copy()
id(B) == id(baboon)
baboon[:, :,] = 0
plt.figure(figsize=(10,10))
plt.subplot(121)
plt.imshow(cv2.cvtColor(baboon, cv2.COLOR_BGR2RGB))
plt.title("baboon")
plt.subplot(122)
plt.imshow(cv2.cvtColor(A, cv2.COLOR_BGR2RGB))
plt.title("array A")
plt.show()

plt.figure(figsize=(10,10))
plt.subplot(121)
plt.imshow(cv2.cvtColor(baboon, cv2.COLOR_BGR2RGB))
plt.title("baboon")
plt.subplot(122)
plt.imshow(cv2.cvtColor(B, cv2.COLOR_BGR2RGB))
plt.title("array B")
plt.show()

### FLIPPING IMAGES
image = cv2.imread("cat.png")
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
plt.show()

width, height,C=image.shape
print ('width, height,C', width, height, C)

array_flip = np.zeros((width, height, C), dtype=np.uint8)
for i,row in enumerate(image):
    array_flip[width-1-i,:,:]=row
plt.figure(figsize=(5,5))
plt.imshow(cv2.cvtColor(array_flip, cv2.COLOR_BGR2RGB))
plt.show()

# OpenCVhas several ways to flip an image, we can use the flip() function; we
# have the input image array. The parameter is the flipCode

#is the value indicating what kind of flip we would like to perform;
#flipcode = 0: flip vertically around the x-axis
#flipcode > 0: flip horizontally around y-axis positive value
#flipcode< 0: flip vertically and horizontally, flipping around both axes negative value
#Let apply different flipcode's in a loop:
for flipcode in [0,1,-1]:
    im_flip = cv2.flip(image, flipcode)
    plt.imshow(cv2.cvtColor(im_flip, cv2.COLOR_BGR2RGB))
    plt.title("flipcode " + str(flipcode))
    plt.show()

# there's also the rotate() function
im_flip = cv2.rotate(image, 0)
plt.imshow(cv2.cvtColor(im_flip, cv2.COLOR_BGR2RGB))
plt.show()

flip = {"ROTATE_90_CLOCKWISE":cv2.ROTATE_90_CLOCKWISE,
        "ROTATE_90_COUNTERCLOCKWISE":cv2.ROTATE_90_COUNTERCLOCKWISE,
        "ROTATE_180": cv2.ROTATE_180}
flip["ROTATE_90_CLOCKWISE"]

for key, value in flip.items():
    plt.subplot(1,2,1)
    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    plt.title("orignal")
    plt.subplot(1,2,2)
    plt.imshow(cv2.cvtColor(cv2.rotate(image,value), cv2.COLOR_BGR2RGB))
    plt.title(key)
    plt.show()

### IMAGE CROPPING
# vertical
upper = 150
lower = 400
crop_top = image[upper: lower,:,:]
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(crop_top, cv2.COLOR_BGR2RGB))
plt.show()
# horizontal
left = 150
right = 400
crop_horizontal = crop_top[: ,left:right,:]
plt.figure(figsize=(5,5))
plt.imshow(cv2.cvtColor(crop_horizontal, cv2.COLOR_BGR2RGB))
plt.show()

### CHANGING PIXELS
array_sq = np.copy(image)
array_sq[upper:lower, left:right, :] = 0

plt.figure(figsize=(10,10))
plt.subplot(1,2,1)
plt.imshow(cv2.cvtColor(image,cv2.COLOR_BGR2RGB))
plt.title("orignal")
plt.subplot(1,2,2)
plt.imshow(cv2.cvtColor(array_sq,cv2.COLOR_BGR2RGB))
plt.title("Altered Image")
plt.show()

start_point, end_point = (left, upper), (right, lower)
image_draw = np.copy(image)
cv2.rectangle(image_draw, pt1=start_point, pt2=end_point, color=(0, 255, 0), thickness=3)
plt.figure(figsize=(5,5))
plt.imshow(cv2.cvtColor(image_draw, cv2.COLOR_BGR2RGB))
plt.show()

### Overlay text on image with putText()

# img: Image array
# text: Text string to be overlayed
# org: Bottom-left corner of the text string in the image
# fontFace: tye type of font
# fontScale: Font scale
# color: Text color
# thickness: Thickness of the lines used to draw a text
# lineType: Line type

image_draw = cv2.putText(img=image, text="Stuff",
                         org=(10,500),
                         color=(255,255,255),
                         fontFace=4,
                         fontScale=5,
                         thickness=2)
plt.figure(figsize=(10,10))
plt.imshow(cv2.cvtColor(image_draw,cv2.COLOR_BGR2RGB))
plt.show()

# Open the image and create a OpenCV Image object called im, convert the image
# from BGR format to RGB format, flip im vertically around the x-axis and
# create an image called im_flip, mirror im by flipping it horizontally around
# the y-axis and create an image called im_mirror, finally plot both images
im = cv2.imread("baboon.png", cv2.COLOR_BGR2RGB)
im_flip = cv2.flip(im, 1)
im_mirror = cv2.flip(im, 0)
plt.figure(figsize=(10,10))
plt.subplot(1,2,1)
plt.imshow(cv2.cvtColor(im_flip,cv2.COLOR_BGR2RGB))
plt.title("flip")
plt.subplot(1,2,2)
plt.imshow(cv2.cvtColor(im_mirror,cv2.COLOR_BGR2RGB))
plt.title("mirror")
plt.show()

