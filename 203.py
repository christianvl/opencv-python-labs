### Pixel transformation

# !wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/lenna.png -O lenna.png
# !wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/baboon.png -O baboon.png
# !wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/goldhill.bmp -O goldhill.bmp
# !wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/cameraman.jpeg -O cameraman.jpeg
# !wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/zelda.png -O zelda.png
# !wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CV0101EN-SkillsNetwork/images%20/images_part_1/mammogram.png -O mammogram.png

import matplotlib.pyplot as plt
import cv2
import numpy as np

# helper function to show images side by side
def plot_image(image_1, image_2,title_1="Orignal", title_2="New Image"):
    plt.figure(figsize=(10,10))
    plt.subplot(1, 2, 1)
    plt.imshow(image_1,cmap="gray")
    plt.title(title_1)
    plt.subplot(1, 2, 2)
    plt.imshow(image_2,cmap="gray")
    plt.title(title_2)
    plt.show()

# helper function to plot histograms side by side
def plot_hist(old_image, new_image,title_old="Orignal", title_new="New Image"):
    intensity_values=np.array([x for x in range(256)])
    plt.subplot(1, 2, 1)
    plt.bar(intensity_values, cv2.calcHist([old_image],[0],None,[256],[0,256])[:,0],width = 5)
    plt.title(title_old)
    plt.xlabel('intensity')
    plt.subplot(1, 2, 2)
    plt.bar(intensity_values, cv2.calcHist([new_image],[0],None,[256],[0,256])[:,0],width = 5)
    plt.title(title_new)
    plt.xlabel('intensity')
    plt.show()

### A histogram counts the number of occurrences of the intensity values of
### pixels, and it's a useful tool for understanding and manipulating images.
### We use cv.calcHist() to generate the histogram. Here are the parameter
### values:

# cv2.calcHist(CV array:[image] this is the image channel:[0],for this course it will always be [None],the number of bins:[L],the range of index of bins:[0,L-1])
# L = 256

### Consider the toy array with intensity values ranging from 0 to 2. We can
### create a histogram. Its first element is the number of zeros in the image
### (in this case, 1); its second element is the number of ones in the image
### (in this case, 5), and so on.
toy_image = np.array([[0, 2, 2], [1, 1, 1], [1, 1, 2]], dtype=np.uint8)
plt.imshow(toy_image, cmap='gray')
plt.show()
print ("toy_image:", toy_image)

### We can use the caclHist function, in this case, we use only three bins as
### there are only three values, and the index of the bins are from 1 to 3.
plt.bar([x for x in range(6)], [1, 5, 2, 0, 0, 0])
plt.show()

plt.bar([x for x in range(6)], [0, 1, 0, 5, 0, 2])
plt.show()

### The histogram is a function where ℎ\[𝑟] where 𝑟∈0,1,2. In the above example ℎ\[0]=1,ℎ\[1]=5 and ℎ\[23

### Histograms are used in grayscale images. Grayscale images are used in many
### applications, including medical and industrial. Color images are split into
### luminance and chrominance. The luminance is the grayscale portion and is
### usually processed in many applications. Consider the following "Gold Hill"
### image:
goldhill = cv2.imread("goldhill.bmp",cv2.IMREAD_GRAYSCALE)
plt.figure(figsize=(10,10))
plt.imshow(goldhill,cmap="gray")
plt.show()

hist = cv2.calcHist([goldhill], [0], None, [256], [0, 256])
intensity_values = np.array([x for x in range(hist.shape[0])])
plt.bar(intensity_values, hist[:,0], width = 5)
plt.title("Bar histogram")
plt.show()

# We can convert it to a probability mass function by normalizing it by the number of pixels:
PMF = hist / (goldhill.shape[0] * goldhill.shape[1])

# plot continuous function
plt.plot(intensity_values,hist)
plt.title("histogram")
plt.show()

# apply histogram to each image color
baboon = cv2.imread("baboon.png")
plt.imshow(cv2.cvtColor(baboon,cv2.COLOR_BGR2RGB))
plt.show()

color = ('blue','green','red')
for i,col in enumerate(color):
    histr = cv2.calcHist([baboon],[i],None,[256],[0,256])
    plt.plot(intensity_values,histr,color = col,label=col+" channel")
    
    plt.xlim([0,256])
plt.legend()
plt.title("Histogram Channels")
plt.show()

### INTENSITY TRANSFORMATION
neg_toy_image = -1 * toy_image + 255

print("toy image\n", neg_toy_image)
print ("image negatives\n", neg_toy_image)
plt.figure(figsize=(10,10))
plt.subplot(1, 2, 1) 
plt.imshow(toy_image,cmap="gray")
plt.subplot(1, 2, 2)
plt.imshow(neg_toy_image,cmap="gray")
plt.show()
print ("toy_image:", toy_image)

### reversing intensity has many applications. Ex.: medical images
image = cv2.imread("mammogram.png", cv2.IMREAD_GRAYSCALE)
cv2.rectangle(image, pt1=(160, 212), pt2=(250, 289), color = (255), thickness=2) 

plt.figure(figsize = (10,10))
plt.imshow(image, cmap="gray")
plt.show()

img_neg = -1 * image + 255
plt.figure(figsize=(10,10))
plt.imshow(img_neg, cmap = "gray")
plt.show()


### BRIGHTNESS AND CONTRAST
alpha = 1 # Simple contrast control
beta = 100   # Simple brightness control   
new_image = cv2.convertScaleAbs(goldhill, alpha=alpha, beta=beta)

plot_image(goldhill, new_image, title_1="Orignal", title_2="brightness control")
plt.figure(figsize=(10,5))
plot_hist(goldhill, new_image, "Orignal", "brightness control")

plt.figure(figsize=(10,5))
alpha = 2# Simple contrast control
beta = 0 # Simple brightness control   # Simple brightness control
new_image = cv2.convertScaleAbs(goldhill, alpha=alpha, beta=beta)

plot_image(goldhill, new_image, "Orignal", "contrast control")

plt.figure(figsize=(10,5))
plot_hist(goldhill, new_image, "Orignal", "contrast control")

plt.figure(figsize=(10,5))
alpha = 3 # Simple contrast control
beta = -200  # Simple brightness control   
new_image = cv2.convertScaleAbs(goldhill, alpha=alpha, beta=beta)
plot_image(goldhill, new_image, "Orignal", "brightness & contrast control")
plt.figure(figsize=(10,5))
plot_hist(goldhill, new_image, "Orignal", "brightness & contrast control")


### HISTOGRAM EQUALIZATION
zelda = cv2.imread("zelda.png",cv2.IMREAD_GRAYSCALE)
new_image = cv2.equalizeHist(zelda)

plot_image(zelda, new_image, "Orignal", "Histogram Equalization")
plt.figure(figsize=(10,5))
plot_hist(zelda, new_image, "Orignal", "Histogram Equalization")


### TRESHHOLDING AND SIMPLE SEGMENTATION
def thresholding(input_img,threshold,max_value=255, min_value=0):
    N,M=input_img.shape
    image_out=np.zeros((N,M),dtype=np.uint8)
        
    for i  in range(N):
        for j in range(M):
            if input_img[i,j]> threshold:
                image_out[i,j]=max_value
            else:
                image_out[i,j]=min_value
                
    return image_out

toy_image
threshold = 1
max_value = 2
min_value = 0
thresholding_toy = thresholding(toy_image, threshold=threshold, max_value=max_value, min_value=min_value)
thresholding_toy

plt.figure(figsize=(10, 10))
plt.subplot(1, 2, 1)
plt.imshow(toy_image, cmap="gray")
plt.title("Original Image")
plt.subplot(1, 2, 2)
plt.imshow(thresholding_toy, cmap="gray")
plt.title("Image After Thresholding")
plt.show()

image = cv2.imread("cameraman.jpeg", cv2.IMREAD_GRAYSCALE)
plt.figure(figsize=(10, 10))
plt.imshow(image, cmap="gray")
plt.show()

hist = cv2.calcHist([goldhill], [0], None, [256], [0, 256])
plt.bar(intensity_values, hist[:, 0], width=5)
plt.title("Bar histogram")
plt.show()

# The cameraman corresponds to the darker pixels, therefore we can set the
# Threshold in such a way as to segment the cameraman. In this case, it looks
# to be slightly less than 90, let’s give it a try:

threshold = 87
max_value = 255
min_value = 0
new_image = thresholding(image, threshold=threshold, max_value=max_value, min_value=min_value)
plot_image(image, new_image, "Orignal", "Image After Thresholding")
plt.figure(figsize=(10,5))
plot_hist(image, new_image, "Orignal", "Image After Thresholding")


# The parameter thresholding type is the type of thresholding we would like to
# perform. For example, we have basic thresholding: cv2.THRESH_BINARY this is
# the type we implemented in the function thresholding, it just a number:

cv2.THRESH_BINARY

# We can apply thresholding to the image as follows:

ret, new_image = cv2.threshold(image,threshold,max_value,cv2.THRESH_BINARY)
plot_image(image,new_image,"Orignal","Image After Thresholding")
plot_hist(image, new_image,"Orignal","Image After Thresholding")

# ret is the threshold value and new_image is the image after thresholding has
# been applied. There are different threshold types, for example,
# cv2.THRESH_TRUNC will not change the values if the pixels are less than the
# threshold value:

ret, new_image = cv2.threshold(image,86,255,cv2.THRESH_TRUNC)
plot_image(image,new_image,"Orignal","Image After Thresholding")
plot_hist(image, new_image,"Orignal","Image After Thresholding")

# We see that the darker elements have not changed and the lighter values are
# set to 255. Otsu's method cv2.THRESH_OTSU avoids having to choose a value and
# determines it automatically, using the histogram.

ret, otsu = cv2.threshold(image,0,255,cv2.THRESH_OTSU)

plot_image(image,otsu,"Orignal","Otsu")

plot_hist(image, otsu,"Orignal"," Otsu's method")

#We assign the first row of pixels of the original array to the new array's
#last row. We repeat the process for every row, incrementing the row number for
#the original array and decreasing the new array's row index assigning the
#pixels accordingly.

ret
