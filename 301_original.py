# Libraries for data processing and visualization:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from imutils import paths
import seaborn as sns
import random
import time
from datetime import datetime

# Libraries for image pre-processing and classification:
import cv2
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

# Libraries for OS and Cloud:
import os
from skillsnetwork import cvstudio

# Download Your Images and Annotations
# Now, let's initialize and download the images from the project you just created in CV Studio.

# Initialize the CV Studio Client
cvstudioClient = cvstudio.CVStudio()
​
# Download All Images
cvstudioClient.downloadAll()

# Get the annotations from the project you downloaded from CV Studio:
annotations = cvstudioClient.get_annotations()
# Let's view the format of the annotations we've just downloaded. The following
# code will display only the first 5 annotations. The annotations will come in
# a JSON file. What you can see is the image name as the key and dog as label
# object.
first_five = {k: annotations["annotations"][k] for k in list(annotations["annotations"])[:5]}
first_five

# Load and Plot and Image We will train and classify your images using the k-NN
# classifier using the OpenCV library. Before we start, let's get the images
# and take a look at some of them.

# We will pick random images and take a look:
random_filename = 'images/' + random.choice(list(annotations["annotations"].keys()))

# Plot, read and show a random image using the cv2.imread and the matplotlib
# library. We will also change the color space to RGB so we can plot it since
# OpenCV reads images as BGR. Early developers at OpenCV chose BGR color format
# because it was the format that was popular among camera manufacturers and
# software providers.
sample_image = cv2.imread(random_filename)
## Convert to RGB
image = cv2.cvtColor(sample_image, cv2.COLOR_BGR2RGB)
## Now plot the image
plt.figure(figsize=(10,10))
plt.imshow(image, cmap = "gray")
plt.show()
## if you plot with sample_image, you will observe a difference in the color space
## uncomment to try it out
# plt.imshow(sample_image)
# plt.show()

# To perform KNN on the dataset, we will need to process the data. I will use
# the sample image to explain each line of code.

# Convert image to grayscale - grayscale simplifies the algorithm and reduces
# computational requirements.
sample_image = cv2.cvtColor(sample_image,cv2.COLOR_BGR2GRAY)
plt.figure(figsize=(10,10))
plt.imshow(sample_image, cmap = "gray")
plt.show()

# Resize image - resizing image helps the algorithm train faster.
sample_image = cv2.resize(sample_image, (32, 32))
plt.imshow(sample_image, cmap = "gray")
plt.show()

# Flatten image - makes the image a numpy array for the algorithm to handle and recognize.
pixels = sample_image.flatten()
pixels

# Repeat the Process Above for All Images We will now repeat the same process
# above to load and process all the images you’ve annotated and label each
# picture. KNN is supervised machine learning algorithm, therefore we have to
# explicitly create labels for the machine.

# Depending on how much data you have, this will take a while to run...

image_paths = list(paths.list_images('images'))
train_images = []
train_labels = []
class_object = annotations['labels']
# loop over the input images
for (i, image_path) in enumerate(image_paths):
    #read image
    image = cv2.imread(image_path)
    #make images gray
    image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    #label image using the annotations
    label = class_object.index(annotations["annotations"][image_path[7:]][0]['label'])
    tmp_label = annotations["annotations"][image_path[7:]][0]['label']
    # resize image
    image = cv2.resize(image, (32, 32))
    # flatten the image
    pixels = image.flatten()
    #Append flattened image to
    train_images.append(pixels)
    train_labels.append(label)
    print('Loaded...', '\U0001F483', 'Image', str(i+1), 'is a', tmp_label)
# Create an array of the train_images and train_labels. OpenCV only identifies
# arrays of type float32 for the training samples and array of shape (label
# size, 1) for the training labels. We can do that by specifying
# astype('float32') on the numpy array of the training samples and convert the
# training labels to integers and reshape the array to (label size, 1). When
# you print the train_labels, the array will look like this [[1], [0], ...,
# [0]]

train_images = np.array(train_images).astype('float32')
train_labels = np.array(train_labels)
train_labels = train_labels.astype(int)
train_labels = train_labels.reshape((train_labels.size,1))
print(train_labels)

# Split data into training and test set with a test size of your choice:
test_size = 0.2
train_samples, test_samples, train_labels, test_labels = train_test_split(
    train_images, train_labels, test_size=test_size, random_state=0)

# To train the KNN model, we will use the cv2.ml.KNearest_create() from the
# OpenCV library. We need to define how many nearest neighbors will be used for
# classification as a hyper-parameter k. This parameter k can be toggled
# with/tuned in the training or model validation process. Fit the training and
# test images and get the accuracy score of the model.

# We will try multiple values of k to find the optimal value for the dataset we
# have. k refers to the number of nearest neighbours to include in the majority
# of the voting process.

# Note: Depending on how large your dataset is, it may take a few seconds to
# run.
start_datetime = datetime.now()

knn = cv2.ml.KNearest_create()
knn.train(train_samples, cv2.ml.ROW_SAMPLE, train_labels)
​
## get different values of K
k_values = [1, 2, 3, 4, 5]
k_result = []
for k in k_values:
    ret,result,neighbours,dist = knn.findNearest(test_samples,k=k)
    k_result.append(result)
flattened = []
for res in k_result:
    flat_result = [item for sublist in res for item in sublist]
    flattened.append(flat_result)
​
end_datetime = datetime.now()
print('Training Duration: ' + str(end_datetime-start_datetime))

# We will get the accuracy value for each value of k i.e., how many percent of
# the images were classified correctly? We will create a confusion matrix for a
# more comprehensive classification model evaluation.

## create an empty list to save accuracy and the cofusion matrix
accuracy_res = []
con_matrix = []
## we will use a loop because we have multiple value of k
for k_res in k_result:
    label_names = [0, 1]
    cmx = confusion_matrix(test_labels, k_res, labels=label_names)
    con_matrix.append(cmx)
    ## get values for when we predict accurately
    matches = k_res==test_labels
    correct = np.count_nonzero(matches)
    ## calculate accuracy
    accuracy = correct*100.0/result.size
    accuracy_res.append(accuracy)
## stor accuracy for later when we create the graph
res_accuracy = {k_values[i]: accuracy_res[i] for i in range(len(k_values))}
list_res = sorted(res_accuracy.items())


# A Quick Guide to the Confusion Matrix

# A confusion matrix is a performance measurement for classification problem.
# It is a table with a combination of predicted and actual values. On the
# y-axis, we have the True label and on the x-axis we have the Predicted label.
# This example will focus on a binary classifier, i.e. a yes or no model.

#  	        Predicted: NO	Predicted: YES
# True: NO	 30	            30
# True: YES	 10	            50

# In this matrix, we can see that there are two classes. For example, if we
# were predicting if an image is a hotdog, "yes" will be a hotdog and "no" will
# be not a hotdog. We have 120 predictions and out of those times, the
# classifier predicted "yes" 80 times and "no" 40 times but really, there were
# 60 "yes"s and 60 "no"s.

# When we talk about confusion matrix, we talk about a few terms:

# True Positive (TP): Our model predicted "yes", and it was actually "yes"
# True Negative (TN): Our model predicted "no", and it was actually "no"
# False Positive (FP): Our model predicted "yes", but it was actually "no"
# False Negative (FN): Our model predicted "no", but it was actually "yes"
# Let's look at it in the context of our example:

#  	           Predicted: NO	Predicted: YES
# True: NO	   TN = 30	        FP = 30	       60
# True: YES	   FN = 10	        TP = 50	       60
#  	                40	             80

# Accuracy is the number the model got right over the total number of
# predictions. This is (TP+TN)/Total Number of predictions.

# Now let's visualize the confusion matrix:

t=0
## for each value of k we will create a confusion matrix
for array in con_matrix:
    df_cm = pd.DataFrame(array)
    sns.set(font_scale=1.4) # for label size
    sns.heatmap(df_cm, annot=True, annot_kws={"size": 16}, fmt = ".0f") # font size
    t += 1
    title = "Confusion Matrix for k equals " + str(t)
    plt.title(title)
    plt.show()
​
# We will plot the accuracy to see which one is highest i.e., what percentage
# of images were classified correctly?

## plot accuracy against
x, y = zip(*list_res)
plt.plot(x, y)
plt.show()

# We will get the best value of k to train the model to test the model on our
# image:
k_best = max(list_res,key=lambda item:item[1])[0]
k_best

# Let's Report Our Results Back to CV Studio
parameters = {
    'k_best': k_best
}
result = cvstudioClient.report(started=start_datetime, completed=end_datetime, parameters=parameters, accuracy=list_res)
​
if result.ok:
    print('Congratulations your results have been reported back to CV Studio!')

# Save the KNN model to a file:
knn.save('knn_samples.yml')

# Check that the knn_samples.yml was saved to your directory.
# Now let's save the model back to CV Studio:
result = cvstudioClient.uploadModel('knn_samples.yml', {'k_best': k_best})
