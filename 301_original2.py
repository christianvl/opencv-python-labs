# Load the Saved KNN model to Classify Images
# Project: Training_an_image_classifier_with_KNN
# Application: k-NN Classifier tester
# You will upload your image and classify it using the K-Nearest Neighbours (KNN) algorithm we saved in the train run.

# Objectives
# This tool contains the following sections:
# Load important libraries
# Load your saved KNN model
# Upload your image
# Classify your image
# Load Important Libraries
# Libraries for data processing and visualization:

import numpy as np
import matplotlib.pyplot as plt

# Libraries for image pre-processing and classification:
import cv2

# Libraries for Computer Vision Studio:
from skillsnetwork import cvstudio

# Setup CV Studio Client, Get the Model and Annotations
# Initialize the CV Studio Client
cvstudioClient = cvstudio.CVStudio()

# Get the annotations from the CV Studio project:
annotations = cvstudioClient.get_annotations()

# Download the model (for k-NN this means samples and format):
model_details = cvstudioClient.downloadModel()

# Get k_best from model details (we reported this previously when training):
k_best = model_details["k_best"]

# Load and re-train model:
## get the model from the cv studio storage
fs = cv2.FileStorage(model_details['filename'], cv2.FILE_STORAGE_READ)
knn_yml = fs.getNode('opencv_ml_knn')

knn_format = knn_yml.getNode('format').real()
is_classifier = knn_yml.getNode('is_classifier').real()
## number of samples
default_k = knn_yml.getNode('default_k').real()
## sample arrays
samples = knn_yml.getNode('samples').mat()
## labels of sample
responses = knn_yml.getNode('responses').mat()
fs.release
knn = cv2.ml.KNearest_create()
knn.train(samples,cv2.ml.ROW_SAMPLE,responses)

# Test Our Model with an Uploaded Image
# Upload your image, and see if it will be correctly classified.

# Instructions on How to Upload an Image:

# Use the upload button and upload an image from your local machine:

# The image will now be in the directory in which you are working in. To read
# the image in a new cell, use the cv2.imread and read its name. For example, I
# uploaded anothercar.jpg into my current working directory -
# cv2.imread("anothercar.jpg").

# Replace your_uploaded_file below with the name of your image as seen in your
# directory:
my_image = cv2.imread("your_uploaded_file.jpg")
## let's see what the image looks like
image = cv2.cvtColor(my_image, cv2.COLOR_BGR2RGB)
plt.imshow(image)
plt.show()

# Convert image to grayscale - grayscale simplifies the algorithm and reduces
# computational requirements.

#make images gray
my_image = cv2.cvtColor(my_image,cv2.COLOR_BGR2GRAY)

# Resize the image to reduce the size:
my_image = cv2.resize(my_image, (32, 32))

# Flatten the image into a numpy array:
pixel_image = my_image.flatten()
pixel_image = np.array([pixel_image]).astype('float32')

# Classify the image and print out the result of the model:
ret,result,neighbours,dist = knn.findNearest(pixel_image,k=k_best)
print(neighbours)
print('Your image was classified as a ' + str(annotations['labels'][int(ret)]))

# When we print out the neigbours, it tells you the k closest classes and uses
# a majority voting process to pick what your image may be classified as.

