### Logistic regression with mini-batch gradient descent

# Required libraries
# Allows us to use arrays to manipulate and store data
import numpy as np
# Used to graph data and loss curves
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
# PyTorch Library
import torch
# Used to help create the dataset and perform mini-batch
from torch.utils.data import Dataset, DataLoader
# PyTorch Neural Network
import torch.nn as nn

# Create class for plotting and the function for plotting

class plot_error_surfaces(object):

    # constructor
    def __init__(self, w_range, b_range, X, Y, n_samples = 30, go = True):
        W = np.linspace(-w_range, w_range, n_samples)
        B = np.linspace(-b_range, b_range, n_samples)
        w, b = np.meshgrid(W, B)
        Z = np.zeros((30, 30))
        count1 = 0
        self.y = Y.numpy()
        self.x = X.numpy()
        for w1, b1 in zip(w, b):
            count2 = 0
            for w2, b2 in zip(w1, b1):
                yhat = 1 / (1 + np.exp(-1 * (w2 * self.x + b2)))
                Z[count1, count2] = -1 * np.mean(self.y * np.log(yhat + 1e-16) + (1 - self.y) * np.log(1 - yhat + 1e-16))
                count2 += 1
            count1 += 1
        self.Z = Z
        self.w = w
        self.b = b
        self.W = []
        self.B = []
        self.LOSS = []
        self.n = 0
        if go == True:
            plt.figure()
            plt.figure(figsize=(7.5,5))
            plt.axes(projection='3d').plot_surface(self.w, self.b, self.Z, rstride=1, cstride=1, cmap='viridis', edgecolor='none')
            plt.title('Loss Surface')
            plt.xlabel('w')
            plt.ylabel('b')
            plt.show()
            plt.figure()
            plt.title("Loss Surface Contour")
            plt.xlabel('w')
            plt.ylabel('b')
            plt.copntour(self.w, self.b, self.Z)
            plt.show()

    def set_para_loss(self, model, loss):
        self.n = self.n + 1
        self.W.append(list(model.parameters())[0].item())
        self.B.append(list(model.parameters())[1].item())
        self.LOSS.append(loss)

    def final_plot(self):
        ax = plt.axes(projection='3d')
        ax.plot_wireframe(self.w, self.b, self.Z)
        ax.scatter(self.W, self.B, self.LOSS, c='r', marker='x', s=200, aplha=1)
        plt.figure()
        plt.contour(self.w, self.b, self.Z)
        plt.scatter(self.W, self.B, c='r', marker='x')
        plt.xlabel('w')
        plt.ylabel('b')
        plt.show()

    def plot_ps(self):
        plt.subplot(121)
        plt.ylim
        plt.plot(self.x[self.y==0], self.y[self.y==0], 'ro', label="training points")
        plt.plot(self.x[self.y==1], self.y[self.y==1]-1, 'o', label="training points")
        plt.plot(self.x, self.W[-1] * self.x + self.B[-1], label="estimated line")
        plt.xlabel('x')
        plt.ylabel('y')
        plt.ylim((-0.1, 2))
        plt.title('Data Space Iteration: ' + str(self.n))
        plt.show()
        plt.subplot(122)
        plt.contour(self.w, self.b, self.Z)
        plt.scatter(self.W, self.B, c='r', marker='x')
        plt.title('Loss Surface Contour Iteration' + str(self.n))
        plt.xlabel('w')
        plt.ylabel('b')

    def PlotStuff(X, Y, model, epoch, leg=True):
        plt.plot(X.numpy(), model(X).detach().numpy(), label=('epoch ' + str(epoch)))
        plt.plot(X.numpy(), Y.numpy(), 'r')
        if leg == True:
            plt.legend()
        else:
            pass

# Setting the seed will allow us to control randomness and give us reproducibility
torch.manual_seed(0)

### Create/load dataset (override __len__ and __getitem__)

# Create the custom Data class inheriting Dataset
class Data(Dataset):

    # constructor
    def __init__(self):
        # X values from -1 to 1 step .1
        self.x = torch.arrange(-1, 1, 0.1).view(-1,1)
        # Y values all set to 0
        self.y = torch.zeros(self.x.shape[0], 1)
        # X values above 0.2 to 1
        self.y[self.x[:,0] > 0.2] = 1
        # create .len attribute to override __len__
        self.len = self.x.shape[0]

    def __getitem__(self, index):
        return self.x[index], self.y[index]

    def __len__(self):
        return self.len


# create data object
data_set = Data()

data_set.x  # view X values
data_set.y  # view y values
len(data_set)  # length of the dataset

x, y, = data_set[0] # x and y for the "n" sample
print ("x = {}, y = {}".format(x, y))

# separate one-dimensional dataset into two classes
plt.plot(data_set.x[data_set.y==0], data_set.y[data_set.y==0], 'ro', label="y=0")
plt.plot(data_set.x[data_set.y==1], data_set.y[data_set.y==1]-1, 'o', label="y=1")
plt.xlabel('x')
plt.legend()


### Create the model and total loss function (cost)
# Create logistic_regression class that inherits nn.Module which is the base class for all neural networks
class logistic_regression(nn.Module):

    # Constructor
    def __init__(self, n_inputs):
        super(logistic_regression, self).__init__()
        # Single layer of Logistic Regression with number of inputs being n_inputs and there being 1 output 
        self.linear = nn.Linear(n_inputs, 1)

    # Prediction
    def forward(self, x):
        # Using the input x value puts it through the single layer defined above then puts the output through the sigmoid function and returns the result
        yhat = torch.sigmoid(self.linear(x))
        return yhat

# We can check the number of features an X value has, the size of the input, or the dimension of X
x, y = data_set[0]
len(x)

# Create a logistic regression object or model, the input parameter is the number of dimensions
model = logistic_regression(1)

# We can make a prediction sigma σ this uses the forward function defined above
x = torch.tensor([-1.0])
sigma = model(x)
sigma

# make a prediction using our data
x, y = data_set[2]
sigma = model(x)
sigma

### Create a plot_error_surfaces object to visualize the data space and the
### learnable parameters space during training: We can see on the Loss Surface
### graph, the loss value varying across w and b values with yellow being high
### loss and dark blue being low loss which is what we want. On the Loss Surface
### Contour graph we can see a top-down view of the Loss Surface graph

# Create the plot_error_surfaces object
# 15 is the range of w
# 13 is the range of b
# data_set[:][0] are all the X values
# data_set[:][1] are all the Y values
get_surface = plot_error_surfaces(15, 13, data_set[:][0], data_set[:][1])

# We define a criterion using Binary Cross Entropy Loss. This will measure the
# difference/loss between the prediction and actual value.
criterion = nn.BCELoss()

# Calculate the loss
x, y = data_set[0]
print ("x = {},  y = {}".format(x, y))
sigma = model(x)
sigma
loss = criterion(sigma, y)
loss

### Setting the batch size using data loader
batch_size = 10
trainloader = DataLoader(dataset=data_set, batch_size=10)
dataset_iter = iter(trainloader)
X, y = next(dataset_iter)
X


### Setting learning rate
# We can set the learning rate by setting it as a parameter in the optimizer
# along with the parameters of the logistic regression model we are training.
# The job of the optimizer, torch.optim.SGD, is to use the loss generated by
# the criterion to update the model parameters according to the learning rate.
# SGD stands for Stochastic Gradient Descent which typically means that the
# batch size is set to 1, but the data loader we set up above has turned this
# into Mini-Batch Gradient Descent.
learning_rate = 0.1
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)


### Mini-Batch gradient descent
# In this case, we will set the batch size of the data loader to 5 and the
# number of epochs to 250. First, we must recreate the get_surface object again
# so that for each example we get a Loss Surface for that model only.
get_surface = plot_error_surfaces(15, 13, data_set[:][0], data_set[:][1], 30)


### Train the model
# First we create an instance of the model we want to train
model = logistic_regression(1)
# We create a criterion which will measure loss
criterion = nn.BCELoss()
# We create a data loader with the dataset and specified batch size of 5
trainloader = DataLoader(dataset = data_set, batch_size = 5)
# We create an optimizer with the model parameters and learning rate
optimizer = torch.optim.SGD(model.parameters(), lr = .01)
# Then we set the number of epochs which is the total number of times we will train on the entire training dataset
epochs=500
# This will store the loss over iterations so we can plot it at the end
loss_values = []
# Loop will execute for number of epochs
for epoch in range(epochs):
    # For each batch in the training data
    for x, y in trainloader:
        # Make our predictions from the X values
        yhat = model(x)
        # Measure the loss between our prediction and actual Y values
        loss = criterion(yhat, y)
        # Resets the calculated gradient value, this must be done each time as it accumulates if we do not reset
        optimizer.zero_grad()
        # Calculates the gradient value with respect to each weight and bias
        loss.backward()
        # Updates the weight and bias according to calculated gradient value
        optimizer.step()
        # Set the parameters for the loss surface contour graphs
        get_surface.set_para_loss(model, loss.tolist())
        # Saves the loss of the iteration
        loss_values.append(loss)
    # Want to print the Data Space for the current iteration every 20 epochs
    if epoch % 20 == 0:
        get_surface.plot_ps()


# We can see the final values of the weight and bias. This weight and bias
# correspond to the orange line in the Data Space graph and the final spot of
# the X in the Loss Surface Contour graph.
w = model.state_dict()['linear.weight'].data[0]
b = model.state_dict()['linear.bias'].data[0]
print ("w = ", w, "b = ", b)


# Now we can get the accuracy of the training data
# Getting the predictions
yhat = model(data_set.x)
# Rounding the prediction to the nearedt integer 0 or 1 representing the classes
yhat = torch.round(yhat)
# Counter to keep track of correct predictions
correct = 0
# Goes through each prediction and actual y value
for prediction, actual in zip(yhat, data_set.y):
    # Compares if the prediction and actualy y value are the same
    if (prediction == actual):
        # Adds to counter if prediction is correct
        correct+=1
# Outputs the accuracy by dividing the correct predictions by the length of the dataset
print ("Accuracy: ", correct / len(data_set) * 100, "%")


# Finally, we plot the Cost vs Iteration graph, although it is erratic it is
# downward sloping.
plt.plot(loss_values)
plt.xlabel("Iteration")
plt.ylabel("Cost")



### Stochastic Gradient Descent
# In this case, we will set the batch size of the data loder to 1 so that the
# gradient descent will be performed for each example this is referred to as
# Stochastic Gradient Descent. The number of epochs is set to 100. Notice that
# in this example the batch size is decreased from 5 to 1 so there would be
# more iterations. Due to this, we can reduce the number of iterations by
# decreasing the number of epochs. Due to the reduced batch size, we are
# optimizing more frequently so we don't need as many epochs. First, we must
# recreate the get_surface object again so that for each example we get a Loss
# Surface for that model only.
get_surface = plot_error_surfaces(15, 13, data_set[:][0], data_set[:][1], 30)



# Train the model
# First we create an instance of the model we want to train
model = logistic_regression(1)
# We create a criterion which will measure loss
criterion = nn.BCELoss()
# We create a data loader with the dataset and specified batch size of 1
trainloader = DataLoader(dataset = data_set, batch_size = 1)
# We create an optimizer with the model parameters and learning rate
optimizer = torch.optim.SGD(model.parameters(), lr = .01)
# Then we set the number of epochs which is the total number of times we will train on the entire training dataset
epochs=100
# This will store the loss over iterations so we can plot it at the end
loss_values = []
# Loop will execute for number of epochs
for epoch in range(epochs):
    # For each batch in the training data
    for x, y in trainloader:
        # Make our predictions from the X values
        yhat = model(x)
        # Measure the loss between our prediction and actual Y values
        loss = criterion(yhat, y)
        # Resets the calculated gradient value, this must be done each time as it accumulates if we do not reset
        optimizer.zero_grad()
        # Calculates the gradient value with respect to each weight and bias
        loss.backward()
        # Updates the weight and bias according to calculated gradient value
        optimizer.step()
        # Set the parameters for the loss surface contour graphs
        get_surface.set_para_loss(model, loss.tolist())
        # Saves the loss of the iteration
        loss_values.append(loss)
    # Want to print the Data Space for the current iteration every 20 epochs
    if epoch % 20 == 0:
        get_surface.plot_ps()


# We can see the final values of the weight and bias. This weight and bias
# correspond to the orange line in the Data Space graph and the final spot of
# the X in the Loss Surface Contour graph.
w = model.state_dict()['linear.weight'].data[0]
b = model.state_dict()['linear.bias'].data[0]
print ("w = ", w, "b = ", b)


# Now we can get the accuracy of the training data
# Getting the predictions
yhat = model(data_set.x)
# Rounding the prediction to the nearedt integer 0 or 1 representing the classes
yhat = torch.round(yhat)
# Counter to keep track of correct predictions
correct = 0
# Goes through each prediction and actual y value
for prediction, actual in zip(yhat, data_set.y):
    # Compares if the prediction and actualy y value are the same
    if (prediction == actual):
        # Adds to counter if prediction is correct
        correct+=1
# Outputs the accuracy by dividing the correct predictions by the length of the dataset
print ("Accuracy: ", correct / len(data_set) * 100, "%")


# Finally, we plot the Cost vs Iteration graph, although it is erratic it is
# downward sloping.
plt.plot(loss_values)
plt.xlabel("Iteration")
plt.ylabel("Cost")



### High learning rate
# In this case, we will set the batch size of the data loder to 1 so that the
# gradient descent will be performed for each example this is referred to as
# Stochastic Gradient Descent. This time the learning rate will be set to .1 to
# represent a high learning rate and we will observe what will happen when we
# try to train. First, we must recreate the get_surface object again so that
# for each example we get a Loss Surface for that model only.
get_surface = plot_error_surfaces(15, 13, data_set[:][0], data_set[:][1], 30)


# train the model
# First we create an instance of the model we want to train
model = logistic_regression(1)
# We create a criterion that will measure loss
criterion = nn.BCELoss()
# We create a data loader with the dataset and specified batch size of 1
trainloader = DataLoader(dataset = data_set, batch_size = 1)
# We create an optimizer with the model parameters and learning rate
optimizer = torch.optim.SGD(model.parameters(), lr = 1)
# Then we set the number of epochs which is the total number of times we will train on the entire training dataset
epochs=100
# This will store the loss over iterations so we can plot it at the end
loss_values = []
# Loop will execute for number of epochs
for epoch in range(epochs):
    # For each batch in the training data
    for x, y in trainloader:
        # Make our predictions from the X values
        yhat = model(x)
        # Measure the loss between our prediction and actual Y values
        loss = criterion(yhat, y)
        # Resets the calculated gradient value, this must be done each time as it accumulates if we do not reset
        optimizer.zero_grad()
        # Calculates the gradient value with respect to each weight and bias
        loss.backward()
        # Updates the weight and bias according to calculated gradient value
        optimizer.step()
        # Set the parameters for the loss surface contour graphs
        get_surface.set_para_loss(model, loss.tolist())
        # Saves the loss of the iteration
        loss_values.append(loss)
    # Want to print the Data Space for the current iteration every 20 epochs
    if epoch % 20 == 0:
        get_surface.plot_ps()


### Notice in this example the due to the high learning rate the Loss Surface
### Contour graph has increased movement over the previous example and also
### moves in multiple directions due to the minimum being overshot. We can see
### the final values of the weight and bias. This weight and bias correspond to
### the orange line in the Data Space graph and the final spot of the X in the
### Loss Surface Contour graph.
w = model.state_dict()['linear.weight'].data[0]
b = model.state_dict()['linear.bias'].data[0]
print ("w = ", w, "b = ", b)



# Now we can get the accuracy of the training data
# Getting the predictions
yhat = model(data_set.x)
# Rounding the prediction to the nearedt integer 0 or 1 representing the classes
yhat = torch.round(yhat)
# Counter to keep track of correct predictions
correct = 0
# Goes through each prediction and actual y value
for prediction, actual in zip(yhat, data_set.y):
    # Compares if the prediction and actualy y value are the same
    if (prediction == actual):
        # Adds to counter if prediction is correct
        correct+=1
# Outputs the accuracy by dividing the correct predictions by the length of the dataset
print ("Accuracy: ", correct / len(data_set) * 100, "%")



# Finally, we plot the Cost vs Iteration graph, although it is erratic it is
# downward sloping.
plt.plot(loss_values)
plt.xlabel("Iteration")
plt.ylabel("Cost")



### Using the following code train the model using a learning rate of .01, 120
### epochs, and batch_size of 1.
get_surface = plot_error_surfaces(15, 13, data_set[:][0], data_set[:][1], 30)

# First we create an instance of the model we want to train
model = logistic_regression(1)
# We create a criterion which will measure loss
criterion = nn.BCELoss()
# We create a data loader with the dataset and specified batch size of 1
trainloader = DataLoader(dataset = data_set, batch_size = 1) # "SET_BATCH_SIZE")
# We create an optimizer with the model parameters and learning rate
optimizer = torch.optim.SGD(model.parameters(), lr = .01) # "SET_LEARNING_RATE")
# Then we set the number of epochs which is the total number of times we will train on the entire training dataset
epochs= 120 # "SET_NUMBER_OF_EPOCHS"
# This will store the loss over iterations so we can plot it at the end
loss_values = []
# Loop will execute for number of epochs
for epoch in range(epochs):
    # For each batch in the training data
    for x, y in trainloader:
        # Make our predictions from the X values
        yhat = model(x)
        # Measure the loss between our prediction and actual Y values
        loss = criterion(yhat, y)
        # Resets the calculated gradient value, this must be done each time as it accumulates if we do not reset
        optimizer.zero_grad()
        # Calculates the gradient value with respect to each weight and bias
        loss.backward()
        # Updates the weight and bias according to calculated gradient value
        optimizer.step()
        # Set the parameters for the loss surface contour graphs
        get_surface.set_para_loss(model, loss.tolist())
        # Saves the loss of the iteration
        loss_values.append(loss)
    # Want to print the Data Space for the current iteration every 20 epochs
    if epoch % 20 == 0:
        get_surface.plot_ps()


# We can see the final values of the weight and bias. This weight and bias
# correspond to the orange line in the Data Space graph and the final spot of
# the X in the Loss Surface Contour graph.
w = model.state_dict()['linear.weight'].data[0]
b = model.state_dict()['linear.bias'].data[0]
print ("w = ", w, "b = ", b)


# Now we can get the accuracy of the training data
# Getting the predictions
yhat = model(data_set.x)
# Rounding the prediction to the nearedt integer 0 or 1 representing the classes
yhat = torch.round(yhat)
# Counter to keep track of correct predictions
correct = 0
# Goes through each prediction and actual y value
for prediction, actual in zip(yhat, data_set.y):
    # Compares if the prediction and actualy y value are the same
    if (prediction == actual):
        # Adds to counter if prediction is correct
        correct+=1
# Outputs the accuracy by dividing the correct predictions by the length of the dataset
print ("Accuracy: ", correct / len(data_set) * 100, "%")


# Finally, we plot the Cost vs Iteration graph, although it is erratic it is
# downward sloping.
plt.plot(loss_values)
plt.xlabel("Iteration")
plt.ylabel("Cost")
