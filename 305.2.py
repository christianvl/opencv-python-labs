# Image Classification with HOG and SVM
# Project: Training_an_image_classifier_with_SVM
# Training Run: SVM classifier tester

# Now that training is done, you can use your model for predictions. As your
# recall Support Vector Machines (SVM) are supervised learning models used for
# classification. We will be using SVM to classify images.

# You will upload your own image and classify using the model from the previous section.
# Objectives

#     This tool contains the following sections:
#         Import Libraries
#         Load saved SVM model
#         Use the best parameters of C, gamma and the Kernel
#         Upload your own image and test the model

# Import Important Libraries and Define Auxilary Functions

# Libraries for data processing and visualization:
import numpy as np
import matplotlib.pyplot as plt

# Libraries for image pre-processing and classification:
import cv2
from sklearn.externals import joblib
from skimage.feature import hog

# Libraries for OS and Cloud:
import os
from skillsnetwork import cvstudio

# Set Up CV Studio Client, Get the Model and Annotations

# We will load the model from the training run.

# Initialize the CV Studio Client
cvstudioClient = cvstudio.CVStudio()

# Get the annotations from CV Studio:
annotations = cvstudioClient.get_annotations()

# Download and load the saved model:
model_details = cvstudioClient.downloadModel()
pkl_filename = model_details['filename']
svm = joblib.load(pkl_filename)

# Practice Exercise - Upload Your Image

# Upload your image, and see if your image will be correctly classified.

# Instructions on How to Upload an Image: Use the upload button and upload an
# image from your local machine: Image

# The image will now be in the directory in which you are working in, to read
# the image, in a new cell, use the cv2.imread and read its name, for example,
# I uploaded anothercar.jpg into my current working directory -
# cv2.imread("anothercar.jpg"). Image

# I have created a function to do it all for you. After importing your image,
# use the function run_svm to classify the object in your image.

def run_svm(image):
    ## show the original image
    orig_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(orig_image)
    plt.show()
    print('\n')
    ## convert the image into a numpy array
    image = np.array(image).astype('uint8')
    ## resize the image to a size of choice
    image = cv2.resize(image, (64, 64))
    ## convert to grayscale to reduce the information in the picture
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ## extract H.O.G features
    hog_features, hog_image = hog(grey_image,
                          visualize=True,
                          block_norm='L2-Hys',
                          pixels_per_cell=(16, 16))
    ## convert the H.O.G features into a numpy array
    image_array = np.array(hog_features)
    ## reshape the array
    image_array = image_array.reshape(1, -1)
    ## make a prediction
    svm_pred = svm.predict(image_array)
    ## print the classifier
    print('Your image was classified as a ' + str(annotations['labels'][int(svm_pred[0])]))

# Now read and classify your image. Replace your_uploaded_file below with the
# name of your image as seen in your directory.

## replace "your_uploaded_file" with your file name
my_image = cv2.imread("your_uploaded_file.jpg")
## run the above function on the image to get a classification
run_svm(my_image)
