# https://cv-studio-accessible.s3.us-south.cloud-object-storage.appdomain.cloud/cats_dogs_images_.zip

# H.O.G. and SVM Image Classification with OpenCV and Computer Vision Learning Studio (CV Studio)
# Project: Training_an_image_classifier_with_SVM
# Training Run: Train SVM image classifier

# Estimated time needed: 60 minutes

# You will learn how to train images with Support Vector Machines (SVM). SVM is a supervised learning model that analyze data used for classification and regression analysis. We will be using SVM to classify images.
# Objectives

# We will be classifying images using Sklearn and Computer Vision Learning Studio (CV Studio). CV Studio is a fast, easy and collaborative open-source Computer Vision tool for teams and individuals. You can upload your datasets and label them yourself. If you created a separate folder for each image class, the tool will do the labeling for you. H.O.G. combined with SVM was one of the ways image classification was done before more advanced methods like Deep Learning became popular.

#     This tool contains the following sections:
#         Import Libraries
#         Image Files and Paths
#         Plotting an Image
#         H.O.G. as a feature descriptor
#         SVM for Image classification
#         Save your model to CVStudio
#         What's Next

# Import Important Libraries and Define Auxilary Functions

# Libraries for data processing and visualization:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from imutils import paths
import seaborn as sns
import random
import time
from datetime import datetime

# Libraries for image pre-processing and classification:
import cv2
from sklearn.externals import joblib
from skimage.feature import hog
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV

# Libraries for OS and Cloud:
import os
from skillsnetwork import cvstudio

# We will load and process every image. Let's go over some concepts:

#         cv2.resize() to resize the image
#         cv2.COLOR_BGR2GRAY() will convert the images to greyscale image
#         hog() will get the H.O.G. features from the image

# We will use this function to read and preprocess the images, the function will be explained in the Histogram of Oriented Gradients (H.O.G.) section.

def load_images(image_paths):
# loop over the input images
    for (i, image_path) in enumerate(image_paths):
        #read image
        image = cv2.imread(image_path)
        image = np.array(image).astype('uint8')
        image = cv2.resize(image, (64, 64))
        grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hog_features, hog_images = hog(grey_image,
                              visualize=True,
                              block_norm='L2-Hys',
                              pixels_per_cell=(16, 16))
        #label image using the annotations
        label = class_object.index(annotations["annotations"][image_path[7:]][0]['label'])
        train_images.append(hog_features)
        train_labels.append(label)

# Download Your Images and Annotations

# We will train and classify them using the SVM classifier using the Sklearn library. Before we start, let's get the images and take a look at some of them.

# Initialize the CV Studio Client
cvstudioClient = cvstudio.CVStudio()
# Download All Images
cvstudioClient.downloadAll()

# Get the annotations from CV Studio:
annotations = cvstudioClient.get_annotations()

# Let's view the format of the annotations we've just downloaded. The following
# code will display only the first 5 annotations. The annotations will come in
# a JSON file. What you can see is the image name as the key and dog as label
# object.
first_five = {k: annotations["annotations"][k] for k in list(annotations["annotations"])[:5]}
first_five

# Histogram of Oriented Gradients (H.O.G.)

# H.O.G. generates a histogram for each localized region. We will pick a random
# image and see how H.O.G. works.
sample_image = 'images/' + random.choice(list(annotations["annotations"].keys()))

# To create H.O.G. features, we will first convert the image to a grayscale image.
sample_image = cv2.imread(sample_image)

# Resize the image to a smaller size to allow the algorithm to run faster and
# convert the images to the grayscale to reduce the number of channels. OpenCV
# reads images as BGR so we will be using that color channel to convert to
# grayscale.

# Early developers at OpenCV chose BGR color format because it was the format
# that was popular among camera manufacturers and software providers.
sample_image = cv2.resize(sample_image, (64, 64))
sample_image = cv2.cvtColor(sample_image, cv2.COLOR_BGR2GRAY)

# Plot the data to look at what it looks like:
plt.imshow(sample_image, cmap=plt.cm.gray)

# Run H.O.G. on the grayscale image to see what it will look like.

# H.O.G. stands for Histogram of Oriented Gradients. It uses the gradient
# orientation of the localized regions of an image and generates a histogram
# for each localized region.

## when we run H.O.G., it returns an array of features and the image/output it produced

## the featurre is what we use to train the SVM model
sample_image_features, sample_hog_image = hog(sample_image,
                              visualize=True,
                              block_norm='L2-Hys',
                              pixels_per_cell=(16, 16))

## lets look at what the H.O.G. feature looks like
plt.imshow(sample_hog_image, cmap=plt.cm.gray)

# Load Images and Generate Training/Testing Dataset

# Initiate a location for saving loaded images:
image_paths = list(paths.list_images('images'))
train_images = []
train_labels = []
class_object = annotations['labels']

# Use the function on the image path:
load_images(image_paths)

# Create an array of the images and use the np.vstack to vertically stack arrays for wrangling.
train_array = np.array(train_images)
train_array = np.vstack(train_array)

# We will reshape the array to (label size, 1). The array will look like this:
# [[1], [0], ..., [0]]
labels_array = np.array(train_labels)
labels_array = labels_array.astype(int)
labels_array = labels_array.reshape((labels_array.size,1))

# Concatenate the images and labels:

train_df = np.concatenate([train_array, labels_array], axis = 1)

# Divide the data into a training and test set:
percentage = 75
partition = int(len(train_df)*percentage/100)
x_train, x_test = train_df[:partition,:-1],  train_df[partition:,:-1]
y_train, y_test = train_df[:partition,-1:].ravel(), train_df[partition:,-1:].ravel()

# Hyperparameters

# The kernel type to be used is a hyperparameter. The most common kernels are
# RBF, poly, or sigmoid. You can also create your own kernel.

# C behaves as a regularization parameter in the SVM. The C parameter trades
# off correct classification of the training examples against the maximization
# of the decision function\u2019s margin. For larger values of C, a smaller
# margin will be accepted if the decision function is better at classifying all
# training points correctly. A lower C will encourage a larger margin,
# therefore a simpler decision function at the cost of accuracy. We select C
# and the best kernel by using the validation data.

# The python dictionary param_grid has different kernels and values of C. We
# can test them using the validation data.

param_grid = {'kernel': ('linear', 'rbf'),'C': [1, 10, 100]}

# gamma is a parameter of the RBF kernel and can be thought of as the spread of
# the kernel and, therefore, the decision region. Low values mean
# \u2018far\u2019 and high values mean \u2018close\u2019. The behaviour of the
# model is very sensitive to the gamma parameter. If gamma is too large, the
# radius of the area of influence of the support vectors only includes the
# support vector itself. We create a Support Vector Classification object.

# Support Vector Machines
base_estimator = SVC(gamma='scale')

# We will train the model and try different kernels and parameter values using the function GridSearchCV. The resulting output will be the model that performs best on the validation data.
start_datetime = datetime.now()
start = time.time()
svm = GridSearchCV(base_estimator, param_grid, cv=5)
#Fit the data into the classifier
svm.fit(x_train,y_train)
#Get values of the grid search
best_parameters = svm.best_params_
print(best_parameters)
#Predict on the validation set
y_pred = svm.predict(x_test)
# Print accuracy score for the model on validation  set. 
print("Accuracy: "+str(accuracy_score(y_test, y_pred)))
end = time.time()
end_datetime = datetime.now()
print(end - start)

# A Quick Guide to the Confusion Matrix

# A confusion matrix is a performance measurement for a classification problem.
# It is a table with a combination of predicted and actual values. On the
# y-axis, we have the True label and on the x-axis we have the Predicted label.
# This example will focus on a binary classifier, i.e. a yes or no model.

# Predicted: NO 	Predicted: YES
# True: NO 	30 	30
# True: YES 	10 	50

# In this matrix, we can see that there are two classes. For example, if we
# were predicting if an image is a hotdog, "yes" will be that it is a hotdog
# and "no" will be that it is not a hotdog. We have 120 predictions and out of
# those times, the classifier predicted "yes" 80 times and "no" 40 times but
# really, there were 60 "yes"s and 60 "no"s.

# When we talk about confusion matrix, we talk about a few terms:

#     True Positive (TP): Our model predicted "yes", and it was actually "yes"
#     True Negative (TN): Our model predicted "no", and it was actually "no"
#     False Positive (FP): Our model predicted "yes", but it was actually "no"
#     False Negative (FN): Our model predicted "no", but it was actually "yes"

# Let's look at it in the context of our example:
#   	Predicted: NO 	Predicted: YES
# True: NO 	TN = 30 	FP = 30 	60
# True: YES 	FN = 10 	TP = 50 	60
#   	40 	80

# Accuracy is the number the model got right over the total number of
# predictions. This is (TP+TN)/Total Number of Predictions.

# Get Confusion Matrix for SVM results:
label_names = [0, 1]
cmx = confusion_matrix(y_test, y_pred, labels=label_names)
df_cm = pd.DataFrame(cmx)
# plt.figure(figsize=(10,7))
sns.set(font_scale=1.4) # for label size
sns.heatmap(df_cm, annot=True, annot_kws={"size": 16}) # font size
title = "Confusion Matrix for SVM results"
plt.title(title)
plt.show()

# Let's Report Our Results Back to CV Studio
parameters = {
    'best_params': best_parameters
}
result = cvstudioClient.report(started=start_datetime, completed=end_datetime, parameters=parameters, accuracy=accuracy_score(y_test, y_pred))
if result.ok:
    print('Congratulations your results have been reported back to CV Studio!')
# Save the SVM model to a file
joblib.dump(svm.best_estimator_, 'svm.joblib')
# Now let's save the model back to CV Studio
result = cvstudioClient.uploadModel('svm.joblib', {'svm_best': svm.best_estimator_})
