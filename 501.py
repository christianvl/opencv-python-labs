# CV Studio logo
# Car Detection with Haar Cascade Classifier
# Car detection with Haar Cascade Classifiers

# You will upload your car image to an already pre-trained Haar cascade
# classifiers and detect the car in the image. Objectives

# Haar Cascade is a machine learning method based on Haar wavelet to identify
# objects in an image or a video. We will use the OpenCV library and CVStudio.
# It is based on the concept of features proposed by Paul Viola and Michael
# Jones in their paper "Rapid Object Detection using a Boosted Cascade of
# Simple Features" in 2001.

#     This tool contains the following sections:
#         Load Images
#         Image preprocessing
#         Load pre-trained classifier
#         Identify a Car in an image
#         Upload your images for Car detection
#         What's Next

# Import important libraries and Define auxilary functions

### install opencv version 3.4.2 for this exercise,
### if you have a different version of OpenCV please switch to the 3.4.2 version
# !{sys.executable} -m pip install opencv-python==3.4.2.16
import urllib.request
import cv2
print(cv2.__version__)
from matplotlib import pyplot as plt
# %matplotlib inline


# Create a function that cleans up and displays the image
def plt_show(image, title="", gray = False, size = (12,10)):
    from pylab import rcParams
    temp = image
    #convert to grayscale images
    if gray == False:
        temp = cv2.cvtColor(temp, cv2.COLOR_BGR2GRAY)
    #change image size
    rcParams['figure.figsize'] = [10,10]
    #remove axes ticks
    plt.axis("off")
    plt.title(title)
    plt.imshow(temp, cmap='gray')
    plt.show()

# Create a function to detect cars in an image
def detect_obj(image):
    #clean your image
    plt_show(image)
    ## detect the car in the image
    object_list = detector.detectMultiScale(image)
    print(object_list)
    #for each car, draw a rectangle around it
    for obj in object_list:
        (x, y, w, h) = obj
        cv2.rectangle(image, (x, y), (x + w, y + h),(255, 0, 0), 2) #line thickness
    ## lets view the image
    plt_show(image)


# Load image Load pre-trained classifier from andrewssobral git repository,
# training takes a long time but prediction is fast.
## read the url
haarcascade_url = 'https://raw.githubusercontent.com/andrewssobral/vehicle_detection_haarcascades/master/cars.xml'
haar_name = "cars.xml"
urllib.request.urlretrieve(haarcascade_url, haar_name)

# ('cars.xml', <http.client.HTTPMessage at 0x7fb98f1e40d0>)

# Get the detector using the cv2.CascadeClassifier() module on the pretrained data set
detector = cv2.CascadeClassifier(haar_name)

# Read in your image
## we will read in a sample image
image_url = "https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/CV0101/Dataset/car-road-behind.jpg"
image_name = "car-road-behind.jpg"
urllib.request.urlretrieve(image_url, image_name)
image = cv2.imread(image_name)

# Plot the image
plt_show(image)

# Run the function on loaded image
detect_obj(image)


# Practice Exercise - Upload your image

# Upload your image and see if your car will be correctly detected.

# Instructions on how to upload an image:
# Use the upload button and upload the image from your local machine
# Image

# The image will now be in the directory in which you are working in. To read
# the image in a new cell, use the cv2.imread function. For example, I uploaded
# anothercar.jpg into my current working directory -
# cv2.imread("anothercar.jpg").

## replace "your_uploaded_file" with your file name
my_image = cv2.imread("your_uploaded_file")

# Run the Cascade classifier on your model to detect the object. How did it do?
detect_obj(my_image)


# Haar Cascade Classifiers are fast and straightforward, you can use
# Convolutional Neural Networks (CNN) and get better performance on your Object
# detection.
